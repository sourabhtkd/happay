from rest_framework import serializers
from book_mgmt import models as book_mgmt_models


class AuthorSerializer(serializers.ModelSerializer):
    dob = serializers.DateField(input_formats=['%d-%b-%Y'], error_messages={
        'invalid': 'Invalid date of birth'
    })
    death_date = serializers.DateField(input_formats=['%d-%b-%Y'], error_messages={
        'invalid': 'Invalid date of date'
    }, required=False, allow_null=True)

    class Meta:
        model = book_mgmt_models.Author
        fields = '__all__'

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        death_date = validated_data.get('death_date', None)
        dob = validated_data.get('dob', None)
        if (death_date and dob) and (death_date < dob):
            raise serializers.ValidationError({'death_date': "Death date cannot be less than date of birth"})
        return validated_data


class AuthorListSerializer(serializers.ModelSerializer):

    class Meta:
        model = book_mgmt_models.Author
        fields = ['id','name']


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = book_mgmt_models.Category
        fields = '__all__'


class BookAddEditSerializer(serializers.ModelSerializer):
    publish_date = serializers.DateField(input_formats=['%d-%b-%Y'], error_messages={
        'invalid': 'Invalid publish date'
    }, required=False, allow_null=True)

    class Meta:
        model = book_mgmt_models.Book
        fields = '__all__'
        depth = 0
        extra_kwargs = {'category': {"error_messages": {"does_not_exist": "category does not exist"}}}


class BookListSerializer(serializers.ModelSerializer):
    publish_date = serializers.DateField(input_formats=['%d-%b-%Y'], error_messages={
        'invalid': 'Invalid publish date'
    }, required=False, allow_null=True)

    class Meta:
        model = book_mgmt_models.Book
        fields = '__all__'
        depth = 1
