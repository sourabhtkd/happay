import rest_framework.exceptions
from django.shortcuts import render
from rest_framework import generics
from rest_framework.decorators import action
from rest_framework.response import Response

from book_mgmt import models as book_mgmt_models
from book_mgmt import serializers as book_mgmt_serializers
from rest_framework import viewsets
from rest_framework import mixins


# Create your views here.
class AuthorListCreateViewSet(mixins.ListModelMixin,
                              mixins.CreateModelMixin,
                              viewsets.GenericViewSet):
    serializer_class = book_mgmt_serializers.AuthorSerializer
    queryset = book_mgmt_models.Author.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return book_mgmt_serializers.AuthorListSerializer
        else:
            return book_mgmt_serializers.AuthorSerializer


class CategoryListCreateViewset(mixins.ListModelMixin,
                                mixins.CreateModelMixin,
                                viewsets.GenericViewSet):
    serializer_class = book_mgmt_serializers.CategorySerializer
    queryset = book_mgmt_models.Category.objects.all()


class BookCreateViewSet(mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        viewsets.GenericViewSet):
    serializer_class = book_mgmt_serializers.BookAddEditSerializer
    queryset = book_mgmt_models.Book.objects.all()

    @action(methods=['GET'], detail=False, url_path='most-sold/author/(?P<author_id>[\d]+)',
            url_name='most-sold-by-author')
    def most_book_sold_by_author(self, *args, **kwargs):
        obj = self.queryset.filter(author_id=kwargs['author_id']).order_by('-sold_count').first()
        if not obj:
            raise rest_framework.exceptions.NotFound()

        serializer = self.get_serializer(obj)
        return Response(data=serializer.data)

    @action(methods=['GET'], detail=False, url_path='author/(?P<author_id>[\d]+)', url_name='by-author')
    def book_list_by_author(self, *args, **kwargs):
        self.queryset = self.queryset.filter(author_id=kwargs['author_id'])
        serializer = self.get_serializer(self.queryset, many=True)
        return Response(data=serializer.data)

    @action(methods=['GET'], detail=False, url_path='most-sold/category/(?P<category_id>[\d]+)', url_name='most-sold-by-category')
    def most_book_sold_by_category(self, *args, **kwargs):
        obj = self.queryset.filter(category_id=kwargs['category_id']).order_by('-sold_count').first()
        if not obj:
            raise rest_framework.exceptions.NotFound()

        serializer = self.get_serializer(obj)
        return Response(data=serializer.data)

    def get_serializer_class(self):
        if self.action == 'create':
            return book_mgmt_serializers.BookAddEditSerializer
        else:
            return book_mgmt_serializers.BookListSerializer

    def list(self, request, *args, **kwargs):
        title = request.GET.get('title', '')
        author_name = request.GET.get('author_name', '')
        if title:
            self.queryset = self.queryset.filter(title__icontains=title)
        if author_name:
            self.queryset = self.queryset.filter(author__name__icontains=author_name)

        return super().list(request, *args, **kwargs)
