from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from book_mgmt import views

app_name = 'book_mgmt'
router = routers.DefaultRouter()
router.register('authors', views.AuthorListCreateViewSet, basename='author')
router.register('categories', views.CategoryListCreateViewset, basename='category')
router.register('books', views.BookCreateViewSet, basename='book')

urlpatterns = [
    path('', include(router.urls))
]
