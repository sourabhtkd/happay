from django.core.validators import MinLengthValidator, MaxLengthValidator
from django.db import models


# skipping fields like created_at, updated_at, is_active
# Create your models here.
class Author(models.Model):
    name = models.CharField(max_length=200)
    phone = models.CharField(max_length=10, validators=[MinLengthValidator(10), MaxLengthValidator(10)])
    dob = models.DateField()
    death_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return "{}:{}".format(self.id, self.name)


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{} :{}".format(self.id, self.name)


class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    publisher = models.CharField(max_length=200)
    publish_date = models.DateField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    price = models.FloatField()
    sold_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "{} : {}".format(self.id, self.title)
