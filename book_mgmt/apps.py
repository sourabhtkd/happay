from django.apps import AppConfig


class BookMgmtConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'book_mgmt'
