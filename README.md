# Happay Assignment

## Tasks
- Add Author
- Add Category
- Add Book
- List Categories
- List Authors
- Get most sold book by author
- Get most book sold by category
- search book by partial title, partial author name or both
- get all books by author id 
- if fails in any condition it should fail gracefully 

## Notes
- Test cases are not included due to time constraints



## Installation

```shell
~$ git clone https://gitlab.com/sourabhtkd/happay.git
~$ cd happay
~$ virtualenv venv -p python3
~$ source venv/bin/activate
~$ pip install -r requirements.txt
~$ python manage.py makemigrations 
~$ python manage.py migrate
~$ python manage.py runserver

```

## APIS

### Author
**List Author Name** <br>
URL: localhost:8000/authors/ <br>
Method: GET <br>
Response, status code 200
```json
[
    {
        "name": "author3"
    },
    {
        "name": "author1"
    }
]
```
**Add Author** <br>
URL: localhost:8000/authors/ <br>
Method: POST <br>

Body
```json
{
    "name":"author3",
    "phone":"9911223345",
    "dob":"21-DEC-1993",
    "death_date":"21-DEC-1994"
}
```
Response, status code 200
```json
{
    "id": 1,
    "dob": "1993-12-21",
    "death_date": "1994-12-21",
    "name": "author3",
    "phone": "9911223345"
}
```

Response, status code 400 
```json
{
    "death_date": [
        "Death date cannot be less than date of birth"
    ]
}
```

### Category
**List Category** <br>
URL: http://localhost:8000/categories/ <br>
Method: GET <br>
Response, status code 200 
```json
[
    {
        "id": 1,
        "name": "category 1"
    },
    {
        "id": 2,
        "name": "category 3"
    }
]
```
**Create Category** <br>
URL: http://localhost:8000/categories/ <br>
Method: POST <br>
Data:
```json
{
    "name":"category 1 "
}
```
Response ,status code 200
```json
[
    {
        "id": 1,
        "name": "category 1"
    }
]
```
Response , status code 400 
```json
{
    "name": [
        "This field may not be blank."
    ]
}
```

### Book
**List Books** <br>
URL: http://localhost:8000/books/ <br>
Method: GET <br>
Response, status code 200
```json
[
    {
        "id": 1,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 24.0,
        "sold_count": 3,
        "author": {
            "id": 1,
            "name": "author3",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1994-12-21"
        },
        "category": {
            "id": 2,
            "name": "category 3"
        }
    },
    {
        "id": 2,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 24.0,
        "sold_count": 3,
        "author": {
            "id": 1,
            "name": "author3",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1994-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    },
    {
        "id": 3,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 4.0,
        "sold_count": 3,
        "author": {
            "id": 2,
            "name": "author1",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1995-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    },
    {
        "id": 4,
        "publish_date": "2004-02-01",
        "title": "third book",
        "publisher": "publisher",
        "price": 4.0,
        "sold_count": 3,
        "author": {
            "id": 2,
            "name": "author1",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1995-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    }
]
```
**Add Book** <br>
URL: http://localhost:8000/books/ <br>
Method: POST <br>
Data:
```json
{
    "title":"first book",
    "author":1,
    "publisher":"publisher",
    "publish_date":"1-FEB-2004",
    "category":2,
    "price":24,
    "sold_count":3
}
```
Response, status code 200
```json
{
    "id": 1,
    "publish_date": "2004-02-01",
    "title": "first book",
    "publisher": "publisher",
    "price": 24.0,
    "sold_count": 3,
    "author": 1,
    "category": 2
}
```

Response, status code 400 
```json
{
    "category": [
        "category does not exist"
    ]
}
```
**List Books Most Sold By Author Id** <br>
URL: http://localhost:8000/books/most-sold/author/author_id <br>
     eg:- http://localhost:8000/books/most-sold/author/2 <br>

Description: will return only one most sold book, in case more than one book 
have same sold count still it will return only one book, we can either
return all books having same sold count or will have to set priority based on 
published_date or other criteria <br>

Method: GET <br>
Response status code 200 
```json
{
    "id": 3,
    "publish_date": "2004-02-01",
    "title": "first book",
    "publisher": "publisher",
    "price": 4.0,
    "sold_count": 3,
    "author": {
        "id": 2,
        "name": "author1",
        "phone": "9911223345",
        "dob": "1993-12-21",
        "death_date": "1995-12-21"
    },
    "category": {
        "id": 1,
        "name": "category 1"
    }
}
```
**List All Books Sold By Author Id** <br>
URL: http://localhost:8000/books/author/author_id <br>
     eg:- http://localhost:8000/books/author/1 <br>
Method: GET <br>
Response , status 200:
```json
[
    {
        "id": 1,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 24.0,
        "sold_count": 3,
        "author": {
            "id": 1,
            "name": "author3",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1994-12-21"
        },
        "category": {
            "id": 2,
            "name": "category 3"
        }
    },
    {
        "id": 2,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 24.0,
        "sold_count": 3,
        "author": {
            "id": 1,
            "name": "author3",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1994-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    }
]
```
**List Books Most Sold By Category Id** <br>
URL: http://localhost:8000/books/most-sold/category/category_id <br>
     eg:- http://localhost:8000/books/most-sold/category/1 <br>

Method: GET <br>
Response, status code 200
```json
{
    "id": 2,
    "publish_date": "2004-02-01",
    "title": "first book",
    "publisher": "publisher",
    "price": 24.0,
    "sold_count": 3,
    "author": {
        "id": 1,
        "name": "author3",
        "phone": "9911223345",
        "dob": "1993-12-21",
        "death_date": "1994-12-21"
    },
    "category": {
        "id": 1,
        "name": "category 1"
    }
}
```
**Search Book By Partial Title** <br>
URL: http://localhost:8000/books/?title=your_title <br>
    eg: http://localhost:8000/books/?title=th <br>
Method: GET <br>
Response, status code 200
```json
[
    {
        "id": 4,
        "publish_date": "2004-02-01",
        "title": "third book",
        "publisher": "publisher",
        "price": 4.0,
        "sold_count": 3,
        "author": {
            "id": 2,
            "name": "author1",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1995-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    }
]
```
**Search Book By Partial Author Name** <br>
URL: http://localhost:8000/books/?author_name=author_name_partial <br>
     eg: http://localhost:8000/books/?author_name=auth <br>
Method: GET <br>
Response, status code 200 
```json
[
    {
        "id": 1,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 24.0,
        "sold_count": 3,
        "author": {
            "id": 1,
            "name": "author3",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1994-12-21"
        },
        "category": {
            "id": 2,
            "name": "category 3"
        }
    },
    {
        "id": 2,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 24.0,
        "sold_count": 3,
        "author": {
            "id": 1,
            "name": "author3",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1994-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    },
    {
        "id": 3,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 4.0,
        "sold_count": 3,
        "author": {
            "id": 2,
            "name": "author1",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1995-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    },
    {
        "id": 4,
        "publish_date": "2004-02-01",
        "title": "third book",
        "publisher": "publisher",
        "price": 4.0,
        "sold_count": 3,
        "author": {
            "id": 2,
            "name": "author1",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1995-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    }
]
```
**Search Book By Partial Title and Partial Author Name (And Operation)** <br>
URL: http://localhost:8000/books/?author_name=author_name_partial&title=title_partial <br>
Method: GET <br>
Response, status code 200 
```json
[
    {
        "id": 1,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 24.0,
        "sold_count": 3,
        "author": {
            "id": 1,
            "name": "author3",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1994-12-21"
        },
        "category": {
            "id": 2,
            "name": "category 3"
        }
    },
    {
        "id": 2,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 24.0,
        "sold_count": 3,
        "author": {
            "id": 1,
            "name": "author3",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1994-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    },
    {
        "id": 3,
        "publish_date": "2004-02-01",
        "title": "first book",
        "publisher": "publisher",
        "price": 4.0,
        "sold_count": 3,
        "author": {
            "id": 2,
            "name": "author1",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1995-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    },
    {
        "id": 4,
        "publish_date": "2004-02-01",
        "title": "third book",
        "publisher": "publisher",
        "price": 4.0,
        "sold_count": 3,
        "author": {
            "id": 2,
            "name": "author1",
            "phone": "9911223345",
            "dob": "1993-12-21",
            "death_date": "1995-12-21"
        },
        "category": {
            "id": 1,
            "name": "category 1"
        }
    }
]
```